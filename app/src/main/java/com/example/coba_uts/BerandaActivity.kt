package com.example.coba_uts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_beranda.*

class BerandaActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beranda)
        b1.setOnClickListener{
            startActivity(Intent(this,DataPemilih::class.java))
        }
        b2.setOnClickListener{
            startActivity(Intent(this,Letak::class.java))
        }
        b3.setOnClickListener{
            startActivity(Intent(this,AboutActivity::class.java))
        }
        b4.setOnClickListener{
            startActivity(Intent(this,MainActivity::class.java))
        }
    }
}
