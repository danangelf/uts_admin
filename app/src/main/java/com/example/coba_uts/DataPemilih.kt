package com.example.coba_uts

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_data_pemilih.*


class DataPemilih : AppCompatActivity() {

    override fun setSupportActionBar(toolbar: Toolbar?) {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_pemilih)


        val database = FirebaseDatabase.getInstance()

        var  myRef : DatabaseReference? = database.getReference("pendataan1")

        // Read Data
        myRef?.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                // looping ketika mengambil data
                // merah ? coba tambahkan ()
                val dataArray = ArrayList<Pendataan>()
                for (i in dataSnapshot.children){
                    val data = i.getValue(Pendataan::class.java)
                    data?.key = i.key
                    data?.let { dataArray.add(it) }
                }
                rvListData.adapter = PendataanAdapter(dataArray, object : PendataanAdapter.OnClick {
                    override fun edit(pendataan: Pendataan?) {
                        val intent = Intent(this@DataPemilih, FormPendataanActivity::class.java)
                        intent.putExtra("pendataan", pendataan)
                        startActivity(intent)
                    }

                    override fun delete(key: String?) {
                        AlertDialog.Builder(this@DataPemilih).apply {
                            setTitle("Hapus Data ini ?")
                            setPositiveButton("Ya") { dialogInterface: DialogInterface, i: Int ->
                                myRef?.child(key.toString())?.removeValue()
//                                Toast.makeText(this@MainActivity, key, Toast.LENGTH_SHORT).show()
                            }
                            setNegativeButton("Tidak", { dialogInterface: DialogInterface, i: Int -> })
                        }.show()
                    }
                })
            }

            override fun onCancelled(error: DatabaseError) {
                // Failed to read value
                Log.w("tag", "Failed to read value.", error.toException())
            }
        })

        btTambahP.setOnClickListener {
            startActivity(Intent(this@DataPemilih, FormPendataanActivity::class.java))
        }
    }
}
